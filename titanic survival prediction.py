#!/usr/bin/env python
# coding: utf-8

# # Importing the dependencies

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score


# # Train Data

# ## Data collection and Processing

# In[2]:


#load the data from csv file to pandas DataFrame
titanic_data = pd.read_csv('train.csv')


# In[3]:


#printing the first 5 rows of the dataFrame
titanic_data.head()


# In[4]:


#number of rows and columns
titanic_data.shape


# In[5]:


#getting some informations about the data
titanic_data.info()


# In[6]:


#check the number of missing values in each column
titanic_data.isnull().sum()


# ## Handling the missing values

# In[7]:


#drop the "Cabin" column from the dataframe
titanic_data = titanic_data.drop(columns='Cabin', axis=1)


# In[8]:


#replacing the missing values in the "Age" column with mean value
titanic_data['Age'].fillna(titanic_data['Age'].mean(), inplace=True)


# In[9]:


#finding the mode value of "Embarked" column
print(titanic_data['Embarked'].mode())


# In[10]:


print(titanic_data['Embarked'].mode()[0])


# In[11]:


#Replacing the missing values in "Embarked" column with mode value
titanic_data["Embarked"].fillna(titanic_data['Embarked'].mode()[0], inplace=True)


# In[12]:


#check the number of missing values in each column
titanic_data.isnull().sum()


# ## Data Analysis

# In[13]:


#getting some statistical measures about the data
titanic_data.describe()


# In[14]:


#Finding the number of people survived and not survived
titanic_data['Survived'].value_counts()


# ## Data Visualization

# In[15]:


sns.set()


# In[16]:


#macking a count plot for "Survived" column
sns.countplot(x='Survived', data=titanic_data)


# In[17]:


titanic_data['Sex'].value_counts()


# In[18]:


#macking a count plot for "Sex" column
sns.countplot(x='Sex', data=titanic_data)


# In[19]:


#Number of survivors gender wise
sns.countplot(x='Sex', hue='Survived', data=titanic_data)


# In[20]:


#macking a count plot for "Pclass" column
sns.countplot(x='Pclass', data=titanic_data)


# In[21]:


#Number of survivors PClass wise
sns.countplot(x='Pclass', hue='Survived', data=titanic_data)


# In[22]:


# 1. Age Distribution
plt.figure(figsize=(10,6))
sns.histplot(titanic_data['Age'], bins=30, kde=True, color='blue').set_title('Age Distribution')
plt.xlabel('Age')
plt.ylabel('Count')
plt.show()


# In[23]:


# 2. Age vs. Survival
plt.figure(figsize=(10,6))
sns.kdeplot(titanic_data['Age'].loc[titanic_data['Survived'] == 1], label='Survived', shade=True)
sns.kdeplot(titanic_data['Age'].loc[titanic_data['Survived'] == 0], label='Not Survived', shade=True).set_title('Age vs. Survival')
plt.xlabel('Age')
plt.ylabel('Density')
plt.legend()
plt.show()


# In[24]:


# 3. Fare Distribution
plt.figure(figsize=(10,6))
sns.histplot(titanic_data['Fare'], bins=40, kde=True, color='green').set_title('Fare Distribution')
plt.xlabel('Fare')
plt.ylabel('Count')
plt.show()


# In[25]:


# 4. Embarked vs. Survival
plt.figure(figsize=(10,6))
sns.countplot(x='Embarked', hue='Survived', data=titanic_data).set_title('Embarked vs. Survival')
plt.xlabel('Embarkation Point')
plt.ylabel('Count')
plt.show()


# In[26]:


# 5. Siblings/Spouses (SibSp)
plt.figure(figsize=(14,6))
plt.subplot(1, 2, 1)
sns.countplot(x='SibSp', hue='Survived', data=titanic_data).set_title('Siblings/Spouses (SibSp) vs. Survival')
plt.xlabel('Number of Siblings/Spouses')
plt.ylabel('Count')


# In[27]:


# Parents/Children (Parch) vs. Survival
plt.subplot(1, 2, 2)
sns.countplot(x='Parch', hue='Survived', data=titanic_data).set_title('Parents/Children (Parch) vs. Survival')
plt.xlabel('Number of Parents/Children')
plt.ylabel('Count')

plt.tight_layout()
plt.show()


# ## Encoding the Categorical Columns

# In[28]:


titanic_data['Sex'].value_counts()


# In[29]:


titanic_data['Embarked'].value_counts()


# In[30]:


#converting categorical columns
titanic_data.replace({'Sex':{'male':0,'female':1}, 'Embarked':{'S':0, 'C':1, 'Q':2 }}, inplace=True)


# In[31]:


titanic_data.head()


# ## Separating features & Target

# In[32]:


x = titanic_data.drop(columns=['PassengerId','Name','Ticket','Survived'],axis=1)


# In[33]:


y = titanic_data['Survived']


# In[34]:


x


# In[35]:


y


# ## Splitting data into training and validation sets

# In[36]:


X_train, X_val, y_train, y_val = train_test_split(x, y, test_size=0.2, random_state=42)


# # Model Training with Linear Regression

# In[37]:


from sklearn.linear_model import LinearRegression
from sklearn.metrics import accuracy_score


# In[38]:


linear_model = LinearRegression()
linear_model.fit(X_train, y_train)


# In[39]:


# Predict on validation data
y_val_pred_continuous = linear_model.predict(X_val)

# Convert continuous predictions to binary classification using threshold of 0.5
y_val_pred = [1 if x >= 0.5 else 0 for x in y_val_pred_continuous]

# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy for Linear Regression: {accuracy*100:.2f}%")


# # Model Training with Polinomial Regression

# In[40]:


from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
from sklearn.metrics import accuracy_score


# In[41]:


# Degree of the polynomial. You can adjust this based on your requirements.
degree = 2

# Create a polynomial regression model
polyreg = make_pipeline(PolynomialFeatures(degree), LinearRegression())

# Fit the model
polyreg.fit(X_train, y_train)


# In[42]:


# Predict on validation data
y_val_pred_continuous = polyreg.predict(X_val)

# Convert continuous predictions to binary classification using threshold of 0.5
y_val_pred = [1 if x >= 0.5 else 0 for x in y_val_pred_continuous]

# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy for Polynomial Regression with degree {degree}: {accuracy*100:.2f}%")


# # Model training with support vector regression

# In[43]:


from sklearn.svm import SVR
from sklearn.metrics import accuracy_score


# In[44]:


# Create an SVR model
svr = SVR()

# Fit the model
svr.fit(X_train, y_train)


# In[45]:


# Predict on validation data
y_val_pred_continuous = svr.predict(X_val)

# Convert continuous predictions to binary classification using threshold of 0.5
y_val_pred = [1 if x >= 0.5 else 0 for x in y_val_pred_continuous]

# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy for Support Vector Regression: {accuracy*100:.2f}%")


# # Model training with Decion tree classifier

# In[46]:


from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score


# In[47]:


# Create a DecisionTreeClassifier
clf = DecisionTreeClassifier()

# Train the model
clf.fit(X_train, y_train)


# In[48]:


# Predict on validation data
y_val_pred = clf.predict(X_val)

# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy for Decision Tree Classifier: {accuracy*100:.2f}%")


# # Model Training With random Forest classifier

# In[49]:


from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score


# In[50]:


# Create a RandomForestClassifier
clf = RandomForestClassifier(n_estimators=100, random_state=42)

# Train the model
clf.fit(X_train, y_train)


# In[51]:


# Predict on validation data
y_val_pred = clf.predict(X_val)

# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy for Random Forest Classifier: {accuracy*100:.2f}%")


# # Model Training with logistic regression

# In[52]:


model = LogisticRegression(max_iter=500)
model.fit(X_train, y_train)


# ## Predict on validation data

# In[53]:


y_val_pred = model.predict(X_val)


# In[54]:


# Calculating the accuracy
accuracy = accuracy_score(y_val, y_val_pred)
print(f"Validation Accuracy: {accuracy*100:.2f}%")


# # Collect & Process Test data

# In[55]:


test_data = pd.read_csv('test.csv')
passenger_ids = test_data['PassengerId']

# Preprocess the test data similar to train data
test_data.drop(columns='Cabin', axis=1, inplace=True)
test_data['Age'].fillna(test_data['Age'].mean(), inplace=True)
test_data["Embarked"].fillna(test_data['Embarked'].mode()[0], inplace=True)
test_data.replace({'Sex': {'male': 0, 'female': 1}, 'Embarked': {'S': 0, 'C': 1, 'Q': 2}}, inplace=True)
test_data.drop(columns=['PassengerId', 'Name', 'Ticket'], axis=1, inplace=True)
test_data.fillna(0, inplace=True)


# In[56]:


test_data.head()


# In[57]:


# Make predictions on the test data using random forest casiffier created
predictions = clf.predict(test_data)


# # Preparing Data for submission

# In[58]:


submission = pd.DataFrame({
    'PassengerId': passenger_ids,
    'Survived': predictions
})

# Save to CSV
submission.to_csv('submission.csv', index=False)
print("Submission file created!")

